import org.junit.Test;

import static org.junit.Assert.*;

public class BalancedBracketsTest {

    //TODO: add tests here
    @Test
    public void testBalancedBrackets() {
        assertEquals(true, BalancedBrackets.hasBalancedBrackets("][[[]][]diuotd[]"));
        assertEquals(false, BalancedBrackets.hasBalancedBrackets("][[]kqu[][tui]]"));
    }

    @Test
    public void testBinarySearch() {
        int[] testArray = {1, 8, 12, 19, 25, 29, 30, 34, 39, 50, 57, 74, 89};

        assertEquals(2, BinarySearch.binarySearch(testArray, 12));
        assertNotEquals(2, BinarySearch.binarySearch(testArray, 19));
        assertEquals(3, BinarySearch.binarySearch(testArray, 19));
        assertEquals(-1, BinarySearch.binarySearch(testArray, 24));
    }

    @Test
    public void testFraction() {
        // FIXME
    }

    @Test
    public void testRomanNumeral() {
        // FIXME
    }
}
